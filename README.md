Procedimiento para crear un Repositorio GitLab en Android Studio (Mac Osx version)

Git es un Software de control de versiones no centralizado, orientado al mantenimiento de versiones de aplicaciones con ficheros de código


Descargar el tutorial en formato de Word (docx) o pdf de este repositorio



Si no tienes instalado Git
============================================

Instalando en Mac
============================================

Hay tres maneras fáciles de instalar Git en un Mac. 

La más sencilla es usar el instalador gráfico de Git, que puedes descargar desde la página de SourceForge http://sourceforge.net/projects/git-osx-installer/

Una alternativa es instalar Git a través de MacPorts (http://www.macports.org). 
Si tienes MacPorts instalado, instala Git con:

$ sudo port install git-core +svn +doc +bash_completion +gitweb

No necesitas añadir todos los extras, pero probablemente quieras incluir +svn en caso de que alguna vez necesites usar Git con repositorios Subversion

La segunda alternativa es Homebrew (http://brew.sh/). 

Si ya tienes instalado Homebrew, instala Git con:

$ brew install git
