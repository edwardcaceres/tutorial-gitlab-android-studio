forks

Digamos que Juanito quiere contribuir al proyecto. El sabe cómo corregirlo o añadir más contenido. 
Como propietario del repositorio me interesa que Juanito pueda enviarme el parche de forma rápida, que no pierda mucho tiempo. 
Si es así ¡Juanito estará encantado de colaborar con nosotros! ;-). 
Además, necesito que el proceso sea ágil, no quiero tener que invertir 5 horas de mi tiempo cada vez que tenga que hacer un merge del trabajo que Juanito me envíe.
¿Cómo resuelve git el problema?
Pepito hace un fork de mi repositorio, para lo que sólo necesito darle permiso de lectura.
Pepito trabaja en SU COPIA (fork) del repositorio. Como es suya, puede hacer lo que quiera, la puede borrar, corromper, 
dinamitar, reescribir la historia del proyecto… nos da lo mismo, es SU COPIA (fork).
Cuando Juanito termina de programar y testear el parche, me avisa de que ya lo tiene y me dice “En la rama parche_de_Juanito de MI COPIA (fork), 
tienes el parche que corrige el Bug XXXX”.
Yo voy a su repositorio, miro lo que ha hecho y si está bien lo incorporo (merge) a mi repositorio, que es el original.
Las ventajas de este proceso son las siguientes:
 Juanito trabaja con SU COPIA. En ningún momento le tengo que dar acceso al repositorio central.
El proceso de incorporación de los cambios de Pepito es muy sencillo. Si no hay conflictos en los ficheros puede que sea tan simple como ejecutar 
un par de comandos git.
Pepito tiene muy fácil contribuir, no le cuesta esfuerzo.
Yo puedo gestionar muy fácilmente las contribuciones de muchas personas
